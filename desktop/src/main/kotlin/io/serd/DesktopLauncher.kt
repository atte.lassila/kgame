package io.serd

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration

object DesktopLauncher {
    @JvmStatic
    fun main(arg: Array<String>) {
        val host = arg.firstOrNull()?.split(":")?.firstOrNull() ?: "localhost"
        val config = LwjglApplicationConfiguration()
        config.title = "KGame"
        config.foregroundFPS = 60
        config.backgroundFPS = 60
        config.height = 640
        config.width = 800
        LwjglApplication(KGame(host), config)
    }
}
