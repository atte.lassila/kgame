package io.serd

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.serd.util.CONN
import io.serd.util.FINISH
import io.serd.util.GameState
import io.serd.util.INPUT
import io.serd.util.Movement
import io.serd.util.PlayerInput
import io.serd.util.STATE
import io.serd.util.calculateChecksum
import io.serd.util.createContents
import io.serd.util.packetChannel
import io.serd.util.readChecksum
import io.serd.util.readData
import io.serd.util.readHeader
import kotlinx.coroutines.experimental.GlobalScope
import kotlinx.coroutines.experimental.channels.consumeEach
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetSocketAddress
import java.util.concurrent.TimeUnit

class KGame(host: String) : ApplicationAdapter() {
    private lateinit var renderer: ShapeRenderer
    private lateinit var socket: DatagramSocket

    private val jsonMapper = ObjectMapper().apply {
        registerModule(KotlinModule())
    }

    private var connected: Boolean = false
    private var playerId: Int = -1
    private var gameState: GameState = GameState()
    private var gameOver: Boolean = false

    private val address = InetSocketAddress(host, 9001)

    override fun create() {
        renderer = ShapeRenderer()
        socket = DatagramSocket()
        connectToServer()
    }

    private fun connectToServer() {
        initializeConnection()
        launchUpdateThread()
    }

    private fun initializeConnection() {
        val connectData = CONN.createContents()
        val connectPacket = DatagramPacket(connectData, connectData.size, address)
        println("Trying to connect to $address")
        socket.send(connectPacket)
        GlobalScope.launch {
            delay(TimeUnit.SECONDS.toMillis(5))
            if (!connected) {
                println("Could not connect to server")
                gameOver = true
            }
        }
     }

    private fun launchUpdateThread() = GlobalScope.launch {
        socket.packetChannel().consumeEach { packet ->
            val receivedChecksum = packet.readChecksum()
            if (receivedChecksum != packet.calculateChecksum()) return@consumeEach
            val headers = packet.readHeader()
            when (headers) {
                is STATE -> {
                    connected = true
                    playerId = headers.playerId
                    gameState = jsonMapper.readValue(packet.readData(), GameState::class.java)
                }
                is FINISH -> gameOver = true
            }
        }
    }


    override fun render() {
        if (gameOver) {
            println("Game over!")
            Gdx.app.exit()
        }
        sendInputs()
        renderGameState()
    }

    private fun sendInputs() {
        if (!connected) return
        val playerInput = PlayerInput(playerId, Movement(
                left = Gdx.input.isKeyPressed(Input.Keys.LEFT),
                right = Gdx.input.isKeyPressed(Input.Keys.RIGHT),
                up = Gdx.input.isKeyPressed(Input.Keys.UP),
                down = Gdx.input.isKeyPressed(Input.Keys.DOWN)
        ))
        val inputData = jsonMapper.writeValueAsBytes(playerInput)
        val data = INPUT.createContents(inputData)
        socket.send(DatagramPacket(data, data.size, address))
    }

    private fun renderGameState() {
        Gdx.gl.glClearColor(1f, 1f, 1f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        renderer.begin(ShapeRenderer.ShapeType.Filled)
        for ((id, player) in gameState.coordinates) {
            renderer.color = generateColor(id)
            renderer.circle(player.x.toFloat(), player.y.toFloat(), 10f)
        }
        renderer.end()
    }

    private val goldenRatio = 0.618033988749895f * 360

    private fun generateColor(id: Int): Color {
        val h = ((((id * 23184) % 360)) + goldenRatio) % 360
        val s = 0.5f
        val v = 0.95f
        return Color().fromHsv(h, s, v)
    }

    override fun dispose() {
        val data = FINISH(playerId).createContents()
        val packet = DatagramPacket(data, data.size, address)
        socket.send(packet)
        renderer.dispose()
        System.exit(0)
    }
}
