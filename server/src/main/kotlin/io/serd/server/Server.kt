package io.serd.server

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.serd.util.CONN
import io.serd.util.FINISH
import io.serd.util.INPUT
import io.serd.util.PlayerConnection
import io.serd.util.PlayerDisconnect
import io.serd.util.PlayerEvent
import io.serd.util.PlayerInput
import io.serd.util.STATE
import io.serd.util.calculateChecksum
import io.serd.util.createContents
import io.serd.util.packetChannel
import io.serd.util.readChecksum
import io.serd.util.readData
import io.serd.util.readHeader
import kotlinx.coroutines.experimental.GlobalScope
import kotlinx.coroutines.experimental.channels.ReceiveChannel
import kotlinx.coroutines.experimental.channels.consumeEach
import kotlinx.coroutines.experimental.channels.produce
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
import java.net.SocketAddress

fun gameLoop() = runBlocking {
    val game = Game()
    val sendSocket = DatagramSocket()
    val receiveSocket = DatagramSocket(port)
    val packetChannel = playerEventChannel(receiveSocket)
    val localAddress = InetAddress.getLocalHost().hostAddress
    println("Server started. Listening on $localAddress:$port")
    handleInputs(packetChannel, game)

    Runtime.getRuntime().addShutdownHook(object : Thread() {
        override fun run() {
            val socket = DatagramSocket()
            dropClients(game.playerAddresses.map { it.value }, socket)
        }
    })

    while (true) {
        sendGameState(game, sendSocket)
        dropClients(game.getAndDropInactivePlayers(), sendSocket)
    }
}

private fun handleInputs(packetChannel: ReceiveChannel<PlayerEvent>, game: Game) = GlobalScope.launch {
    while (true) {
        packetChannel.poll()?.let { game.handleEvent(it) }
    }
}

private fun sendGameState(game: Game, sendSocket: DatagramSocket) {
    val data = game.gameStateData()
    game.playerAddresses.forEach { (id, address) ->
        val packetData = STATE(id).createContents(data)
        val packet = DatagramPacket(packetData, packetData.size, address)
        sendSocket.send(packet)
    }
}
private fun dropClients(addresses: List<SocketAddress>, sendSocket: DatagramSocket) {
    addresses.forEach { address ->
        val packetData = FINISH().createContents()
        val packet = DatagramPacket(packetData, packetData.size, address)
        sendSocket.send(packet)
    }
}

private fun playerEventChannel(socket: DatagramSocket): ReceiveChannel<PlayerEvent> = GlobalScope.produce {
    socket.packetChannel().consumeEach { packet ->
        val receivedChecksum = packet.readChecksum()
        if (receivedChecksum != packet.calculateChecksum()) return@consumeEach
        val header = packet.readHeader()
        val event = when (header) {
            is CONN -> PlayerConnection(packet.socketAddress)
            is INPUT -> jsonMapper.readValue(packet.readData(), PlayerInput::class.java)
            is FINISH -> PlayerDisconnect(header.assignedId)
            else -> null
        }
        event?.let { channel.send(it) }
    }
}
