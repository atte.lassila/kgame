package io.serd.server

import io.serd.util.GameState
import io.serd.util.Player
import io.serd.util.PlayerConnection
import io.serd.util.PlayerEvent
import io.serd.util.PlayerInput
import java.net.InetSocketAddress
import java.net.SocketAddress
import java.util.Random

class Game {

    private val random = Random()
    var lastPlayerEvents: Map<Int, Long> = emptyMap()
    var playerAddresses: Map<Int, InetSocketAddress> = emptyMap()
    var gameState: GameState = GameState()

    fun handleEvent(event: PlayerEvent) {
        when (event) {
            is PlayerConnection -> addPlayer(event.socketAddress)
            is PlayerInput -> registerInput(event)
        }
    }

    private fun addPlayer(address: SocketAddress) {
        val inetAddress = address as InetSocketAddress
        val id = generateUniqueId()
        println("New player added with id: $id, address: $inetAddress")
        val (x, y) = generateCoordinates()
        val player = Player(id, x, y)
        lastPlayerEvents += id to System.currentTimeMillis()
        playerAddresses += id to inetAddress
        gameState = gameState.copy(coordinates =  gameState.coordinates + (id to player))
    }

    /**
     * Process movement from player.
     */
    private fun registerInput(input: PlayerInput) {
        var (id, x, y) = gameState.coordinates[input.id] ?: return
        with(input.movement) {
            if (left) {
                x = Math.max(0, x - 1)
            }
            if (right) {
                x = Math.min(800, x + 1)
            }
            if (up) {
                y = Math.min(640, y + 1)
            }
            if (down) {
                y = Math.max(0, y - 1)
            }
        }
        lastPlayerEvents += id to System.currentTimeMillis()
        gameState = gameState.copy(coordinates = gameState.coordinates + (id to Player(id, x, y)))
    }

    /**
     * Removes inactive players from game state and returns their addresses (inactive = 5 seconds since last message)
     */
    fun getAndDropInactivePlayers(): List<SocketAddress> {
        val currTime = System.currentTimeMillis()
        val inactiveIds = lastPlayerEvents.filter { (_, time) -> currTime - time > 5000 }.map { it.key }
        val addresses = inactiveIds.mapNotNull { playerAddresses[it] }
        lastPlayerEvents -= inactiveIds
        playerAddresses -= inactiveIds
        gameState = gameState.copy(coordinates = gameState.coordinates - inactiveIds)
        return addresses
    }

    private fun generateCoordinates(): Pair<Int, Int> =
            random.nextInt(800) to random.nextInt(640)

    private fun generateUniqueId(): Int {
        var id = random.nextInt(100)
        while (playerAddresses.keys.contains(id)) {
            id = random.nextInt(id)
        }
        return id
    }

    fun gameStateData(): ByteArray {
        return jsonMapper.writeValueAsBytes(gameState)
    }
}
