package io.serd.server

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule

val jsonMapper = ObjectMapper().apply {
    registerModule(KotlinModule())
}

const val port = 9001


