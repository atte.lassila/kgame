package io.serd.util

import java.net.SocketAddress

data class GameState(val coordinates: Map<Int, Player> = emptyMap())

data class Player(val id: Int, val x: Int, val y: Int)

data class Movement(val left: Boolean, val right: Boolean, val up: Boolean, val down: Boolean)

sealed class PlayerEvent
data class PlayerInput(val id: Int, val movement: Movement) : PlayerEvent()
data class PlayerConnection(val socketAddress: SocketAddress) : PlayerEvent()
data class PlayerDisconnect(val id: Int) : PlayerEvent()
