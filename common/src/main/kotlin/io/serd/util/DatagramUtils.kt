package io.serd.util

import kotlinx.coroutines.experimental.GlobalScope
import kotlinx.coroutines.experimental.channels.ReceiveChannel
import kotlinx.coroutines.experimental.channels.produce
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.nio.ByteBuffer
import java.util.zip.CRC32

const val CHECKSUM_SIZE = 8
const val HEADER_SIZE = 2
const val PACKET_SIZE = 3000

/**
 * Classes inhering this sealed class are the headers we use to distinguish each packet
 */
sealed class Header(val code: Int)

object CONN : Header(1)
class CONN_ACCEPT(val assignedId: Int) : Header(2)
object INPUT : Header(3)
class STATE(val playerId: Int) : Header(4)
class FINISH(val assignedId: Int = -1) : Header(5)


/**
 * Generates a ready-to-be-sent byte array, with a checksum, and headers set to corresponding values for each [Header] type.
 */
fun Header.createContents(data: ByteArray = byteArrayOf()): ByteArray {
    val headers = ByteArray(HEADER_SIZE)
    headers[0] = code.b
    headers[1] = when (this) {
        is CONN_ACCEPT -> assignedId.b
        is STATE -> playerId.b
        is FINISH -> assignedId.b
        else -> 0
    }
    val packetBytes = ByteBuffer.allocate(PACKET_SIZE - CHECKSUM_SIZE).apply {
        put(headers)
        put(data)
    }.array()
    val checksum = calculateChecksum(packetBytes)
    return ByteBuffer.allocate(PACKET_SIZE).apply {
        putLong(checksum)
        put(packetBytes)
    }.array()
}

/**
 * Uses Java's [CRC32] to calculate a 32-bit CRC of a byte array's contents.
 */
fun calculateChecksum(bytes: ByteArray): Long {
    val checksum = CRC32()
    checksum.update(bytes)
    return checksum.value
}

/**
 * Async channel producing DatagramPackets. This is to hide DatagramSocket's blocking behavious
 */
fun DatagramSocket.packetChannel(): ReceiveChannel<DatagramPacket> = GlobalScope.produce {
    while(true) {
        val packet = receivePacket()
        channel.send(packet)
    }
}

/**
 * Hides byte array buffer handling implementation from DatagramSocket
 */
fun DatagramSocket.receivePacket(): DatagramPacket {
    val buffer = ByteArray(PACKET_SIZE)
    val packet = DatagramPacket(buffer, PACKET_SIZE)
    this.receive(packet)
    return packet
}

/**
 * Reads the bytes from [CHECKSUM_SIZE] to [CHECKSUM_SIZE] + [HEADER_SIZE] into a [Header] object.
 */
fun DatagramPacket.readHeader(): Header {
    val i = CHECKSUM_SIZE
    val byte = data[i]
    val secondaryHeader = data[i + 1].i
    return when (byte) {
        1.b -> CONN
        2.b -> CONN_ACCEPT(secondaryHeader)
        3.b -> INPUT
        4.b -> STATE(secondaryHeader)
        5.b -> FINISH(secondaryHeader)
        else -> throw IllegalArgumentException("Illegal header for packet!")
    }
}

/**
 * Reads the first 8 bytes of a [DatagramPacket]'s byte array, which contain the checksum.
 */
fun DatagramPacket.readChecksum(): Long {
    val bytes = data.copyOfRange(0, CHECKSUM_SIZE)
    val buffer = ByteBuffer.allocate(8)
    buffer.put(bytes)
    buffer.flip()
    return buffer.long
}

/**
 * Calculates a checksum for the headers and data of a [DatagramPacket]'s contents. Used to match against the checksum
 * received in the first 8 bytes.
 */
fun DatagramPacket.calculateChecksum(): Long {
    val bytes = data.copyOfRange(CHECKSUM_SIZE, PACKET_SIZE)
    return calculateChecksum(bytes)
}

/**
 * Reads a [DatagramPacket]'s byte array from the start of the data section.
 */
fun DatagramPacket.readData(): ByteArray {
    return data.copyOfRange(CHECKSUM_SIZE + HEADER_SIZE, this.length)
}

/**
 * Extension properties for easy conversions between ints and bytes.
 */

val Byte.i inline get() = toInt()
val Int.b inline get() = toByte()
